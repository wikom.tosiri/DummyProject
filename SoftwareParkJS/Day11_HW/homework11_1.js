const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')


const app = new Koa()
const router = new Router()

// EJS initizalization


render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})


router.get('/', async ctx => {

  const mysql = require('mysql2/promise');

  // create the connection
  const connection = await mysql.createConnection(
    {
      host: 'localhost',
      user: 'root',
      database: 'codecamp'
    }
  );
  // query database
  const [rows] = await connection.query('SELECT * FROM user')

  let temp = JSON.parse((JSON.stringify(rows)));

  console.log(temp)
  await ctx.render('landing',{key: temp})
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
