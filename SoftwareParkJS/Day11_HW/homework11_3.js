const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')
const util = require('util')

const {myModule_from_database} = require('./model/from_database')
// const {myModule_from_file} = await require('./model/from_file')

const app = new Koa()
const router = new Router()

// EJS initizalization


render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})









// myModule_from_database.func1(); // func1
// myModule_from_file.func1();


// ทำ MiddleWare
async function dummyMiddleware(ctx, next) {
    if (ctx.body != null) {
        // console.log(ctx.body)
        let dateTemp = new Date()
        dateTemp = '' + dateTemp.getFullYear()+'-' + (dateTemp.getMonth()+1) +'-'+ dateTemp.getDate() +' ' +dateTemp.getHours()+':' + dateTemp.getMinutes()+':' + dateTemp.getSeconds()+''

        console.log('DEBUGING' + dateTemp)
        var temp = { data: ctx.body, additionalData: { userID: 1, dateTime: dateTemp } }

        console.log(temp)
        ctx.body = temp
    }
    else {
        await next()
    }
}



app.use(serve(path.join(__dirname, 'public')))
app.use(router.allowedMethods())

app.use(dummyMiddleware)
app.use(router.routes())
app.use(dummyMiddleware)


app.listen(3000)
