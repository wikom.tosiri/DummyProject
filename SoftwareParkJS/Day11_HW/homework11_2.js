const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')
const util = require('util')

const readfile = util.promisify(fs.readFile)


const app = new Koa()
const router = new Router()

// EJS initizalization


render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


router.get('/', async ctx => {

    const mysql = require('mysql2/promise');

    // create the connection
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            database: 'codecamp'
        }
    );
    // query database
    const [rows] = await connection.query('SELECT * FROM user')

    let temp = JSON.parse((JSON.stringify(rows)));

    console.log(temp)
    await ctx.render('landing', { key: temp })
})


router.get('/from_database', async (ctx, next) => {

    const mysql = require('mysql2/promise');

    // create the connection
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            database: 'codecamp'
        }
    );
    // query database
    const [rows] = await connection.query('SELECT * FROM user')

    let temp = JSON.parse((JSON.stringify(rows)));

    ctx.body = rows
    await next()

})

router.get('/from_file', async (ctx, next) => {
    let temp = await readfile('./homework2_1.json', 'utf-8')
    console.log(typeof temp)
    ctx.body = JSON.parse(temp)
    console.log(typeof ctx.body)
    await next()
})

// ทำ MiddleWare
async function dummyMiddleware(ctx, next) {
    if (ctx.body != null) {
        // console.log(ctx.body)
        let dateTemp = new Date()
        dateTemp = '' + dateTemp.getFullYear()+'-' + (dateTemp.getMonth()+1) +'-'+ dateTemp.getDate() +' ' +dateTemp.getHours()+':' + dateTemp.getMinutes()+':' + dateTemp.getSeconds()+''

        console.log('DEBUGING' + dateTemp)
        var temp = { data: ctx.body, additionalData: { userID: 1, dateTime: dateTemp } }

        console.log(temp)
        ctx.body = temp
    }
    else {
        await next()
    }
}



app.use(serve(path.join(__dirname, 'public')))
app.use(router.allowedMethods())

app.use(dummyMiddleware)
app.use(router.routes())
app.use(dummyMiddleware)


app.listen(3000)
