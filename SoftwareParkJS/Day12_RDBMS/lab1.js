// get the client

async function dummyfunction() {
  const mysql = require('mysql2/promise');
  // create the connection
  const connection = await mysql.createConnection(
    {
      host: 'localhost',
      user: 'root',
      database: 'codecamp'
    }
  );
  // query database
  const [rows, fields] = await connection.query('SELECT * FROM user');
  console.log(rows[0]);
  // console.log(fields);

}
dummyfunction();


