const Koa = require('koa') //เป็นคลาส
const Router = require('koa-router') //ไอ้หมอนี้ก็เป็นคลาส
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')


const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
   })   

router.get('/', async ctx => {
        await ctx.render('index')
})


router.get('/index', async ctx => {
        await ctx.render('index')
})

router.get('/portfolio', async ctx => {
        await ctx.render('portfolio')
})

router.get('/skill', async ctx => {
        await ctx.render('skill')
})

router.get('/contact', async ctx => {
        await ctx.render('contact')
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)