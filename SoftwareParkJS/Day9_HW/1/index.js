const fs = require('fs')
function writeDemo1(text) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', text, 'utf8', function (err) {
            if (err)
                reject(err);
            else
                resolve();
        });
    });
}
function readDemo1() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/head.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
                console.log(dataDemo1);
        });
    });
}

function readDemo2() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/body.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
                console.log(dataDemo1);

        });
    });
}
function readDemo3() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/leg.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
                console.log(dataDemo1);

        });
    });
}

function readDemo4() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/feet.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
                console.log(dataDemo1);

        });
    });
}


async function dummyfunction()
{
    let dummyArray = await Promise.all([readDemo1(), readDemo2(),readDemo3(),readDemo4()])
    console.log(dummyArray);

    let dummyString = dummyArray[0]+'\n'+dummyArray[1]+'\n'+dummyArray[2]+'\n'+dummyArray[3]
    writeDemo1(dummyString);
}
dummyfunction();


//   writeDemo1().then(function(){
//     return readDemo1();
//   }).then(function(dataDemo1){
//     return writeDemo2(dataDemo1);
//   }).then(function(data){
//     console.log(data);
//   }).catch(function(error){
//     console.error(error)
//   });
