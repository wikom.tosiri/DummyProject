const fs = require('fs')
function writeDemo1(text) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot2.txt', text, 'utf8', function (err) {
            if (err)
                reject(err);
            else
                resolve();
        });
    });
}
function readDemo1() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/head.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
            console.log(dataDemo1);
        });
    });
}

function readDemo2() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/body.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
            console.log(dataDemo1);

        });
    });
}
function readDemo3() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/leg.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
            console.log(dataDemo1);

        });
    });
}

function readDemo4() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./homework8_1/feet.txt', 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
            console.log(dataDemo1);

        });
    });
}


async function dummyfunction() {
    try {
        let w1 = await readDemo1();
        console.log(w1);
        let w2 = await readDemo2();
        console.log(w2);
        let w3 = await readDemo3();
        console.log(w2);
        let w4 = await readDemo4();
        console.log(w2);
        let w5 = w1+'\n'+w2+'\n'+w3+'\n'+w4
        console.log(w5);

        writeDemo1(w5);
      } catch (error) {
        console.error(error);
      }
    
}
dummyfunction();


//   writeDemo1().then(function(){
//     return readDemo1();
//   }).then(function(dataDemo1){
//     return writeDemo2(dataDemo1);
//   }).then(function(data){
//     console.log(data);
//   }).catch(function(error){
//     console.error(error)
//   });
