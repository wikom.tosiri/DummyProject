const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')


const app = new Koa()
const router = new Router()

// EJS initizalization


render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})


router.get('/', async ctx => {

  const mysql = require('mysql2/promise');

  // create the connection
  const connection = await mysql.createConnection(
    {
      host: 'localhost',
      user: 'root',
      database: 'hw13'
    }
  );
  // query database
  const [rows] = await connection.query(`SELECT SUM(courses.price) as 'TotalPrice'
  FROM enrolls
  RIGHT JOIN courses ON enrolls.course_id = courses.id
  WHERE enrolls.enrolled_at IS NOT NULL`)

  const [rows2] = await connection.query(`SELECT students.name , SUM(courses.price) AS 'Total'
  FROM enrolls
  RIGHT JOIN courses ON enrolls.course_id = courses.id
  RIGHT JOIN students on students.id = enrolls.student_id
  WHERE enrolls.enrolled_at IS NOT NULL
  
  group by enrolls.student_id`)

  
  let temp = JSON.parse((JSON.stringify(rows)));
  let temp2 = JSON.parse((JSON.stringify(rows2)));


  console.log(temp)
  console.log(temp2)

  await ctx.render('landing',{key: temp, key2: temp2})
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
