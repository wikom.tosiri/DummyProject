const peopleSalary = [{"id":"1001","firstname":"Luke","lastname":"SkyWalker","company":"Walk Disney","salary":"40000"},{"id":"1002","firstname":"Tony","lastname":"Stark","company":"Marvel","salary":"1000000"},{"id":"1003","firstname":"Somchai","lastname":"Jaidee","company":"Love2work","salary":"20000"},{"id":"1004","firstname":"Monkey D","lastname":"Luffee","company":"One Price","salary":"9000000"}];

/* สร้างตาราง */
    let key,val,sub_key,sub_val;

    $('#myTable').append($('<tr>')); 
    for(key in peopleSalary[0]) /* วนหา Key แต่ละอันโดยยึดจากคีย์แรกอันเดียว*/
    {
        $('#myTable').append($('<th>'+key+'</th>')); 
    }
    $('#myTable').append($('</tr>')); 

    for(key in peopleSalary) /*วนหาค่าของแต่ละ Key*/
    {
        val = peopleSalary[key];
        $('#myTable').append($('<tr>'));
        
        for(sub_key in val)
        {
            let tempforappend = "";

            if(sub_key === 'salary')
            {
                /*$('#myTable').append($('<ol>'));*/
                tempforappend += '<td><ol>';
                sub_key = [Math.floor(val[sub_key]*1),Math.floor(val[sub_key]*1.1),Math.floor(val[sub_key]*1.1*1.1)]
                sub_key.forEach(function(salaryPerYear) 
                {
                    /*$('#myTable').append($('<li>'+salaryPerYear+'</li>'));*/
                    tempforappend += '<li>'+salaryPerYear+'</li>';
                });
                /*$('#myTable').append($('</ol>'));*/
                tempforappend += '</ol></td>';
                $('#myTable').append($(tempforappend));

            }
            
            else
            {
                sub_val = val[sub_key];
                $('#myTable').append($('<td>'+sub_val+'</td>'));
            }
            
        }
    }
    $('#myTable').append($('</tr>')); /* เขียน Header ได้แล้ว */
