const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')


const app = new Koa()
const router = new Router()

// EJS initizalization


render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})


router.get('/', async ctx => {

  const mysql = require('mysql2/promise');

  // create the connection
  const connection = await mysql.createConnection(
    {
      host: 'localhost',
      user: 'root',
      database: 'hw13'
    }
  );
  // query database
  const [rows] = await connection.query(`SELECT instructors.name FROM courses
  right JOIN instructors ON instructors.id = courses.teach_by
  where courses.id IS null`)

  const [rows2] = await connection.query(`SELECT courses.name AS 'Course' 
  FROM instructors 
  RIGHT JOIN courses ON instructors.id = courses.teach_by
  WHERE instructors.name IS NULL`)

  
  let temp = JSON.parse((JSON.stringify(rows)));
  let temp2 = JSON.parse((JSON.stringify(rows2)));


  console.log(temp)
  console.log(temp2)

  await ctx.render('landing',{key: temp, key2: temp2})
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
