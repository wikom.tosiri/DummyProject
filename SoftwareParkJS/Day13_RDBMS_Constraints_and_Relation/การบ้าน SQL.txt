
-- ข้อ 1.1
SELECT instructors.name
FROM `courses`
right JOIN instructors ON instructors.id = courses.teach_by
where courses.id IS null


-- ข้อ1.2
SELECT courses.name AS 'Course' 
FROM `instructors` 
RIGHT JOIN courses ON instructors.id = courses.teach_by
WHERE instructors.name IS NULL


-- ข้อ 2
-- เพิ่มนักเรียน สิบคน
INSERT INTO `students` (`id`, `name`, `created_at`) 
VALUES (NULL, 'จัญโอชา ปวดอึ', CURRENT_TIMESTAMP), 
(NULL, 'นกน้อย นกทั้งปี', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย2 นกทั้งปี2', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย3 นกทั้งปี3', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย4 นกทั้งปี4', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย5 นกทั้งปี5', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย6 นกทั้งปี6', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย7 นกทั้งปี7', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย8 นกทั้งปี8', CURRENT_TIMESTAMP),
(NULL, 'นกน้อย9 นกทั้งปี9', CURRENT_TIMESTAMP)

-- ลงทะเบียนให้เด็ก
INSERT INTO `enrolls` (`student_id`, `course_id`, `enrolled_at`) VALUES ('1', '2', CURRENT_TIMESTAMP), 
('2', '3', CURRENT_TIMESTAMP),
('3', '4', CURRENT_TIMESTAMP),
('4', '5', CURRENT_TIMESTAMP),
('5', '6', CURRENT_TIMESTAMP),
('6', '7', CURRENT_TIMESTAMP),
('7', '8', CURRENT_TIMESTAMP),
('8', '9', CURRENT_TIMESTAMP),
('9', '9', CURRENT_TIMESTAMP),
('8', '8', CURRENT_TIMESTAMP)

-- มีคอร์สไหนบ้างที่มีคนเรียน (ห้ามแสดงชื่อคอร์สซ้ำ)
SELECT DISTINCT courses.name as 'Courses'
FROM `enrolls`
RIGHT JOIN courses ON enrolls.course_id = courses.id
WHERE enrolls.enrolled_at IS NOT NULL

-- มีคอร์สไหนบ้างที่ไม่มีคนเรียน
SELECT DISTINCT courses.name as 'Courses'
FROM `enrolls`
RIGHT JOIN courses ON enrolls.course_id = courses.id
WHERE enrolls.enrolled_at IS NULL


-- ข้อสาม
-- หาว่ามีคอร์สไหนบ้างที่มีคนเรียน และให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น

SELECT DISTINCT courses.name,instructors.name,courses.price
FROM enrolls
JOIN courses on enrolls.course_id = courses.id
JOIN instructors on instructors.id = courses.teach_by;

-- หาว่ามีคอร์สไหนบ้างที่ไม่มีคนเรียน และให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น

SELECT DISTINCT courses.name,instructors.name,courses.price
FROM courses
LEFT JOIN enrolls on enrolls.course_id = courses.id
LEFT JOIN instructors on instructors.id = courses.teach_by  
LEFT JOIN students on students.id = enrolls.student_id
WHERE students.id IS NULL

-- ต่อจากข้อบนเพิ่มเงื่อนไขคือให้เอามาเฉพาะคอร์สที่มีคนสอน
SELECT DISTINCT courses.name,instructors.name,courses.price
FROM courses
LEFT JOIN enrolls on enrolls.course_id = courses.id
LEFT JOIN instructors on instructors.id = courses.teach_by  
LEFT JOIN students on students.id = enrolls.student_id
WHERE students.id IS NULL AND instructors.name IS NOT NULL