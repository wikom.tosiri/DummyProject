function cloneobj(o) {
    let out, v, key;
    out = Array.isArray(o) ? [] : {};
    for (key in o) {
        v = o[key];
        out[key] = (typeof v === "object" && v !== null) ? cloneobj(v) : v;
    }
    return out;
 }

 let input = [1,2,3] 
 let newObj = cloneobj(input);

    newObj[0] = 9;

 console.log(input);
 console.log(newObj);