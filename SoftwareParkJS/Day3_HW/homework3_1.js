'use strict'
fetch('homework2_1.json').then(response => {
    return response.json();
 })
 .then(employees => {
    addAdditionalField(employees);

    /* สร้างตาราง */
let key,val,sub_key,sub_val;

$('#myTable').append($('<tr>')); 
for(key in employees[0]) /* วนหา Key แต่ละอันโดยยึดจากคีย์แรกอันเดียว*/
{
    $('#myTable').append($('<th>'+key+'</th>')); 
}
$('#myTable').append($('</tr>')); 

for(key in employees) /*วนหาค่าของแต่ละ Key*/
{
    val = employees[key];
    $('#myTable').append($('<tr>'));
    
    for(sub_key in val)
    {
        let tempforappend = "";

        if(sub_key === 'nextSalary')
        {
            /*$('#myTable').append($('<ol>'));*/
            tempforappend += '<td><ol>';
            val[sub_key].forEach(element => {
            tempforappend += '<li>'+element+'</li>';
            });
            /*$('#myTable').append($('</ol>'));*/
            tempforappend += '</ol></td>';
            $('#myTable').append($(tempforappend));

        }
        
        else
        {
            sub_val = val[sub_key];
            $('#myTable').append($('<td>'+sub_val+'</td>'));
        }
        
    }
}
$('#myTable').append($('</tr>')); /* เขียน Header ได้แล้ว */

 })
 .catch(error => {
    console.error('Error:', error);
 });
 
/* สร้างฟังก์ชั่น */
function addYearSalary(row){
    row.yearSalary = row.salary*12;
}
function addNextSalary(row){
    row.nextSalary = [Math.floor(row.salary),Math.floor(row.salary*1.1),Math.floor(row.salary*1.1*1.1)];
}

function addAdditionalField(temp){
    temp.forEach(element => {
        addYearSalary(element);
        addNextSalary(element);
    });
}

